﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMove : MonoBehaviour {
    public float startTime = 0.0f;
    private Animator animator;

    void Start()
    {
        //get Animator component
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        //set start parameter to true
        //Ifwe have passed the start time
        if (Time.time >= startTime)
        {
            animator.SetTrigger("Start");
        }
    }

    void Destroy()
    {
        Destroy(gameObject);

    }

    void OnCollisionEnter(Collision col)
    {
        //transition to die animation
        animator.SetTrigger("Hit");
    }



}
