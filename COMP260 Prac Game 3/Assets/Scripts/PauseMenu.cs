﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    #region Menu
    public GameObject pauseMenu;
    public GameObject optionsPanel;
    public Dropdown qualityDropdown;
    public Dropdown resolutionDropdown;
    public Toggle fullScreenToggle;
    public Slider sfxVolumeSlider;
    public Slider musicVolumeSlider;
    #endregion

    public AudioSource music;
    private bool gamePaused = true;

    // Use this for initialization
    void Start()
    {
        setPause(gamePaused);
        optionsPanel.SetActive(false);

        #region Options
        #region Quality_Options
        ///fill quality options using a for loop + .Add|.AddOptions
        qualityDropdown.ClearOptions();
        List<string> qualityNames = new List<string>();
        for (int i = 0; i < QualitySettings.names.Length; i++)
        {
            qualityNames.Add(QualitySettings.names[i]);
        }
        qualityDropdown.AddOptions(qualityNames);

        #endregion  

        #region ScreenResolution_options
        ///Fill the list of available resolutions & Set current resolution
        resolutionDropdown.ClearOptions();
        List<string> resolutions = new List<string>();
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            resolutions.Add(Screen.resolutions[i].ToString());
        }
        resolutionDropdown.AddOptions(resolutions);

        //Set current resolution
        int currentResolution = 0;
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            if (Screen.resolutions[i].width == Screen.width &&
                Screen.resolutions[i].height == Screen.height)
            {
                currentResolution = i;
                break;
            }
        }
        resolutionDropdown.value = currentResolution;


        #endregion

        #region Volume_options
        if(PlayerPrefs.HasKey("sfxVolume"))
        {
            AudioListener.volume = PlayerPrefs.GetFloat("sfxVolume");
        }
        else
        {
            //ie the first time the game is loaded without cookies -- use default value
            AudioListener.volume = 1;
        }
        music.ignoreListenerVolume = true;

        if (PlayerPrefs.HasKey("musicVolume"))
        {
            music.volume = PlayerPrefs.GetFloat("musicVolume");
        }
        else
        {
            //ie the first time the game is loaded without cookies -- use default value
            music.volume = 1;
        }
        #endregion  
        #endregion
    }

    // Update is called once per frame
    void Update()
    {
        if (!gamePaused && Input.GetKeyDown(KeyCode.Escape))
        {
            setPause(true);
        }
    }
    private void setPause(bool p)
    {
        //activate pause menu
        gamePaused = p;
        pauseMenu.SetActive(gamePaused);
        Time.timeScale = gamePaused ? 0 : 1;
    }

    public void OnPressedPlay()
    {
        //resume game
        setPause(false);
    }

    public void OnPressedQuit()
    {
        Application.Quit();
    }

    public void OnPressedOptions()
    {
        pauseMenu.SetActive(false);
        optionsPanel.SetActive(true);

        //Set the current quality value, as defaults
        qualityDropdown.value = QualitySettings.GetQualityLevel();
        //Set the current volume value, as defaults
        sfxVolumeSlider.value = AudioListener.volume;
        //Set the current volume value, as defaults
        musicVolumeSlider.value = music.volume;

    }

    public void OnPressedCancel()
    {
        pauseMenu.SetActive(true);
        optionsPanel.SetActive(false);
    }

    public void OnPressedApply()
    {
        //Apply the changes to settings
        //Quality
        QualitySettings.SetQualityLevel(qualityDropdown.value);
        //Resolution & Fullscreeen
        Resolution res = Screen.resolutions[resolutionDropdown.value];
        Screen.SetResolution(res.width, res.height, fullScreenToggle.isOn);
        //Volume_sfx
        AudioListener.volume = sfxVolumeSlider.value;
        PlayerPrefs.SetFloat("sfxVolume", AudioListener.volume);
        //Volume_music
        music.volume = musicVolumeSlider.value;
        PlayerPrefs.SetFloat("musicVolume", music.volume);

        pauseMenu.SetActive(true);
        optionsPanel.SetActive(false);

    }
}
