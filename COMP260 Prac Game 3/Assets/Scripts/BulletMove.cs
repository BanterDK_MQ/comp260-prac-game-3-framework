﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    public float speed = 10.0f;

    public Vector3 dir;
    private Rigidbody rb;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = speed * dir;
    }

    void OnCollisionEnter(Collision col)
    {
        //Destroy bullet
        //Destroy(gameObject);
        gameObject.SetActive(false);
    }

    void OnEnable()
    {
        Invoke("Destroy", 2f);

    }

    void Destroy()
    {
        gameObject.SetActive(false);
    }

    //prevent double disabling
    void OnDisable()
    {
        CancelInvoke();
    }


}
