﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FireBullet : MonoBehaviour
{

    public BulletMove bulletPrefab;
    public GameObject go_bullet;
    public float rateOfFire;
    public float timeSinceLastShot = 0f;

    public int pooledBullets;
    public List<GameObject> bulletsList;
    public bool willGrow = true;

    private float fireTime = 2f;



    //void Awake()
    //{
    //    current = this;
    //}

    void Start()
    {
        Invoke("Shoot", fireTime);

        bulletsList = new List<GameObject>();
        for (int i = 0; i < pooledBullets; i++)
        {
            GameObject obj = Instantiate(go_bullet);
            obj.SetActive(false);
            bulletsList.Add(obj);
        }
    }

    public GameObject GetPooledObjects()
    {
        for (int i = 0; i < bulletsList.Count; i++)
        {
            //shoot a bullet if it is inactive in hierarchy
            if (!bulletsList[i].activeInHierarchy)
            {
                return bulletsList[i];
            }
        }
        //if the value isn't a hard preset
        if (willGrow)
        {
            GameObject obj = Instantiate(go_bullet);
            bulletsList.Add(obj);
            return obj;
        }
        return null;
    }

    void Update()
    {
        //check if a bullet is fired
        //if so, shooot

        if (Time.timeScale == 0)
        { return; }

        if (timeSinceLastShot > rateOfFire)
        {
            if (Input.GetButton("Fire1"))
            {
                Shoot();
                timeSinceLastShot = 0;
            }
        }
    }
    void FixedUpdate()
    {
        timeSinceLastShot += Time.deltaTime;
    }

    void Shoot()
    {
        GameObject obj = GetPooledObjects();

        if (obj == null) return;

        obj.transform.position = transform.position;
        obj.transform.rotation = transform.rotation;
        obj.SetActive(true);

        BulletMove bullet = obj.GetComponent<BulletMove>();
        //create a ray towards the mouse location
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        bullet.dir = ray.direction;

    }
}
